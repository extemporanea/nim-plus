


import nim, os, json
import matplotlib.pyplot as plt 

def name(array):

	return ''.join([str(i) for i in array])

def bestmoves(k):

	array = [int(i) for i in bin(k)[2:]]

	state = nim.makestate(array, True)
	
	newmoves = []

	for i in state.avail:

		value = bestmovesdict.get(int(name(i),2),[])

		if value == []:
			newmoves.append([int(name(array),2), int(name(i),2)])
			
	return newmoves
	
# databasename = "decidability/bestmoves.json"
# filename = "decidability/lastbestmoves"

# if not os.path.isfile(databasename):
#	with open(databasename, 'w') as file:
#		file.write('{\n  "0": []\n}')	
	
# if not os.path.isfile(filename):
# 	with open(filename, 'w') as file:
#		file.write('')

# with open(databasename, 'r') as file:
#	bestmovesdict = json.load(file)

bestmovesdict = { 0 : [] }

moves = []

for k in range(1,131072):

	moves.extend(bestmoves(k))
	bestmovesdict[k] = bestmoves(k)
#	with open(databasename, 'w') as file:
#		json.dump(bestmovesdict, file, indent=2)

# for k in bestmovesdict:
#	print(k, ' : ', bestmovesdict[k])

# print(moves)

x = []
y = []

for i in moves:
	x.append(i[0])
	y.append(i[1])

s = .1

fig, ax = plt.subplots() 
ax.scatter(x, y, s, color="black") 
plt.axis('off')
plt.show()




