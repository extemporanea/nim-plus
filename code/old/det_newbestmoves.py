from module import nimplus

import matplotlib.pyplot as plt

def update(state):

	newmoves = []
	newbestmoves = []

	dep_num = nimplus.arrtonum(state.array)

	for i in state.avail:
		
		arr_num = nimplus.arrtonum(i)
		newmoves.append([dep_num,arr_num])

		if dictionary.get(arr_num) == 'L':
			newbestmoves.append([dep_num,arr_num])
			dictionary[dep_num] = 'W'

	return newmoves, newbestmoves


level = 7
moves = []
bestmoves = []
dictionary = { 0 : 'L' }

for k in range(1,2 ** level):

	array = [int(i) for i in bin(k)[2:]]
	state = nimplus.makestate(array, True)
	newmoves, newbestmoves = update(state)
	moves.extend(newmoves)
	bestmoves.extend(newbestmoves)
	dictionary[k] = dictionary.get(k,'L')


for i in dictionary:
	print(i, ' : ', dictionary[i])


x = []
y = []

for k in bestmoves:
	x.append(k[0])
	y.append(k[1])

X = []
Y = []

for k in moves:
	if k not in bestmoves:
		X.append(k[0])
		Y.append(k[1])	
	

s = 10
fig = plt.figure(figsize = (6,6)) # default is (8,6)
ax = fig.add_subplot(111, aspect = 'equal')
ax.scatter(x, y, s, color = "red", alpha = 1)
# ax.scatter(X, Y, s, color = "black", alpha = 1)
ax.set_xlim((0,2 ** level))
ax.set_ylim((0,2 ** level))
plt.axis('off')
plt.show()

 










