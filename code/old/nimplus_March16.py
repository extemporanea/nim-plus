from random import choices
from math import log, floor

def clean(array):

	for i in range(len(array)):
		if array[0] == 0 and len(array) != 1:
			array.pop(0)
						
	return array
		
def parity(array):

	for i in range(len(array)):
		array[i] %= 2

	return array
				
class makestate:

	def __init__(self, code, fast = True, plus = True):
		
		self.fast = fast
		self.plus = plus
		
		if type(code) is list:
			code = parity(code) if fast
			self.array = clean(code)
			self.number = self.number() 
		elif type(code) is int:
			self.fast = True
			self.array = clean([int(i) for i in bin(code)[2:]])
			self.number = code
		
		self.name = ','.join([str(i) for i in self.array])
		self.length = len(self.array)
		
	def __hash__(self):
	
		return hash(self.name)

	def __eq__(self, other):

		return True if self.name == other.name else False

	def number(self):

		if self.fast:
			return int(''.join([str(i) for i in self.array]), 2)
		else:
			None

	def win(self):
	
		array = self.array.copy()
		l = self.length	
		nimsum = 0
		for i in range(len(array)):
			if array[i] % 2 != 0:	
				nimsum ^= l-i
					
		return False if nimsum == 0 else True
					
	def config(self):

		config = []	
	
		for i in range(self.length):
			for j in range(self.array[-i-1]):
				config.append([k for k in range(i+1)])

		return config
					
	def set_avail(self, best = False):

		self.avail = []
		l = self.length

		for i in range(l):
			if self.array[i] != 0:
				for k1 in range(l-i+1):
					for k2 in range(k1,l-i-k1):
						newa = self.array.copy()
						newa[i] -= 1
						if k1 != 0:
							newa[l-k1] += 1
						if k2 != 0 and self.plus:
							newa[l-k2] += 1
						news = makestate(new_a, \
						self.fast, self.plus)
						news.include(self.avail, best)

	def include(self, avail, best = False):
	
		if (best and not self.win()) or not best:
			if self.fast or not self.plus:
				if self not in avail:
					avail.append(self)
			else:
				avail.append(self)

class makeplayer:

	def __init__(self, brains = False, smarts = False, \
	wits = False, spy = True, beads = 42):

		self.brains = brains
		self.smarts = smarts
		self.wits = wits
		self.beads = beads
		self.spy = spy
		
		self.wins = 0
		self.notebook = {}
		self.lastmove = None
		self.boxes = {} if not self.wits else self.set_boxes()
		
	def __eq__(self, other):

		if alice.brains.copy() == bob.brains.copy()\
		and alice.smarts.copy() == bob.smarts.copy()\
		and alice.wits.copy() == bob.wits.copy()\
		and alice.spy.copy() == bob.spy.copy():
			alice.boxes = bob.boxes
		
		return True if self.name == other.name else False
	
	def set_boxes():
	
		self.boxes = {}
		
	def learns(self, winner, loser):
	
		if self.brains: reinforce(self, winner, loser)
		if self.smarts: prune(self, loser)
		if self.wits: pass
		
	def goodmoves(self, state):

		goodmoves = []
		beads = self.boxes.get(state, None)
		
		if beads != None:
			for color in range(len(beads)):
				if beads[color] > self.beads:
					state.set_avail()
					move = state.avail[color]
					goodmoves.append(move)

		return goodmoves

class makegame:

	def __init__(self, level = 3, matches = 1, \
	starter = None, fast = True, plus = True):
	
		self.level = level
		self.matches = matches
		self.starter = starter
		self.plus = plus
		self.fast = fast
		
		code = [1 for i in range(0, self.level)]
		self.start = makestate(code, fast, plus)
		
	def play(self, alice, bob):

		if alice.brains == bob.brains\
		and alice.smarts == bob.smarts\
		and alice.wits == bob.wits\
		and alice.spy == bob.spy:
			alice.boxes = bob.boxes
	
		for i in range(self.matches):

			if self.starter is not None:
				player = self.starter
			else:
				player = choices([alice,bob])[0]
			other = bob if player == alice else alice
					
			winner, loser = match(player, other, self.start)

			alice.learns(winner, loser)
			bob.learns(winner, loser)
			
			alice.notebook = {}
			bob.notebook = {}
			
def match(player, other, start):

	state = start
		
	while state != finish:

		state.set_avail()
		
		if state not in player.boxes: #this must go in player set_boxes
			player.boxes[state] = [player.beads for i in state.avail]
			if other.boxes != player.boxes and (other.smarts or other.brains):
				other.boxes[state] = [player.beads for i in state.avail]

		oldstate = state
		state = choices(state.avail, weights = player.boxes[state])[0]
		move = oldstate, state
		
		player.notebook[move] = player.notebook.get(move,0) + 1
		
		if player.boxes[oldstate].count(0) != len(player.boxes[oldstate])-1:
			player.lastmove = move

		if state == finish:
			winner, loser, = player, other
			winner.wins += 1

		player, other = other, player
		
	return winner, loser

def reinforce(self, winner, loser):

	if self.spy or self == winner:

		for move in winner.notebook:

			move[0].set_avail()
			color = move[0].avail.index(move[1])
			change = self.boxes[move[0]][color] + 1
			if change < 2 * self.beads:
				self.boxes[move[0]][color] = change

	if self.spy or self == loser:

		for move in loser.notebook:
			move[0].set_avail()
			color = move[0].avail.index(move[1])	
			change = self.boxes[move[0]][color] - 1	
			if change > 0:
				self.boxes[move[0]][color] = change
		
def prune(self, loser):

	last = loser.lastmove
	last[0].set_avail()
	color = last[0].avail.index(last[1])
	box = self.boxes[last[0]]
	
	for i in range(len(box)):
		if i == color:
			box[i] = 0
		else:
			box[i] += 1

	
finish = makestate([0])


