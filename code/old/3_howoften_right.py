print('--------------------------')
print('   HOW OFTEN ALICE WINS   ')
print('     WHEN SHE STARTS      ')
print('--------------------------')


import random

LEVEL = 3
GAMES = 100000
WALICE = 0

LEVEL += 1

for g in range(GAMES):
	
	STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
	
### This one is a list of number of possible ways to cross bars in each line

#	if g == 0: print(STATES)
	
	k = 0
	
	while STATES !=[]:
	
		POSSIB = [sum([i for i in range(len(j)+1)])  for j in STATES]

		R_LINE = random.choices(STATES, weights=POSSIB)[0]		
		X = random.choices(R_LINE, weights=[1 for i in range(len(R_LINE))])[0]
		WEIG_Y = []
		for i in range(len(R_LINE)):
			if i != X: WEIG_Y.append(1)
			else: WEIG_Y.append(2)
		Y = random.choices(R_LINE, weights=WEIG_Y)[0]
		
		if X < Y: X,Y = Y,X
		L_SIDE = [i for i in range(len(R_LINE[:X]))]
		R_SIDE = [i for i in range(len(R_LINE[Y+1:]))]
		STATES.remove(R_LINE)
		if L_SIDE != []:
			STATES.append(L_SIDE)
		if R_SIDE != []:
			STATES.append(R_SIDE)
		
		k += 1
		k = k % 2
		
	if k == 1: WALICE += 1
	
print('Alice won', WALICE/GAMES*100,'% of games')

