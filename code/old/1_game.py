print('--------------------------------------')
print('   ALICE AND BOB PLAY A STUPID GAME   ')
print('--------------------------------------')

import random

LEVEL = 2
LEVEL += 1

STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
PLAYER = ['Alice', 'Bob']
def GENDER(k):
    return "She" if k == 0 else "He"
k = random.choices([0, 1], weights = [1/2,1/2])[0]

while STATES !=[]:
	
	print(PLAYER[k],"'s turn to play. %s has to choose one line from these:" %(GENDER(k))) 
	for i in range(len(STATES)): print(STATES[i])

	WEIGHT = [1 for j in range(len(STATES))]
	CHOICE = random.choices(STATES, weights=WEIGHT)[0]

	print('%s chooses:' %(GENDER(k)))	
	print(CHOICE)
	
	HEIGHT = [1 for i in range(len(CHOICE))]
	X = random.choices(CHOICE, weights=HEIGHT)[0]
	Y = random.choices(CHOICE, weights=HEIGHT)[0]
	if Y < X:
		X, Y = Y, X
	L_SIDE = [i for i in range(len(CHOICE[:X]))]
	R_SIDE = [i for i in range(len(CHOICE[Y+1:]))]
	STATES.remove(CHOICE)
	if L_SIDE != []:
		STATES.append(L_SIDE)
	if R_SIDE != []:
		STATES.append(R_SIDE)
	
	print('%s crosses all numbers between' %(GENDER(k)), X, 'and', Y) 
	print('------------------------------------------------------------')

	k += 1
	k = k % 2

print('And the winner is %s !!!' %("Alice" if k == 1 else "Bob"))

