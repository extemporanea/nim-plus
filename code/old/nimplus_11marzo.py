from random import choices
from math import log, floor


def clean(array):

	for i in range(len(array)):
		if array[0] == 0:
			array.pop(0)
	if array == []:
		array = [0]
	return array
		

def odd(array):

	for i in range(len(array)):
		array[i] %= 2
	return array
	
	
def arrtonum(array):

	array = odd(array)
	return int(''.join([str(i) for i in array]), 2)
	

def numtoarr(number):

	return [int(i) for i in bin(number)[2:]]
	
	
def makeconfiguration(array):

	configuration = []
	
	for i in range(len(array)):
		for j in range(array[-i-1]):
			configuration.append([k for k in range(i+1)])
			
	return(configuration)


def makearray(configuration, fast = True):

	length = 0
	for i in configuration:
		if len(i) > length:
			length = len(i)
		
	array = [configuration.count([i for i in range(j)]) for j in range(1, length+1)]
	array.reverse()
	if fast:
		array = odd(array) 
	array =  clean(array)

	return array	
	

def winning(array):

	array = clean(odd(array))
	l = len(array)
	nim_code = []
	digits = floor(log(len(array), 2)) + 1

	for i in range(l):
		if array[i] != 0:
			value = [int(i) for i in bin(l-i)[2:]]
			for k in range(len(value),digits):
				value = [0]+value		
			nim_code.append(value)

	status = [0 for i in range(digits)]

	for k in range(digits):
		for i in range(len(nim_code)):
			status[k] += nim_code[i][k]
		status[k] = status[k] % 2

	return False if clean(status) == [0] else True


def available(array, fast = True, best = False):

	avail = []

	configuration = makeconfiguration(clean(array))
	
	for line in configuration:
		for bar_x in line:
			for bar_y in line:
				if bar_y >= bar_x:
					newconfiguration = configuration.copy()
					l_side = [i for i in range(len(line[:bar_x]))]
					r_side = [i for i in range(len(line[bar_y+1:]))]
					newconfiguration.remove(line)
					if l_side != []:
						newconfiguration.append(l_side)
					if r_side != []:
						newconfiguration.append(r_side)
					newarray = makearray(newconfiguration, fast)
					newarray = clean(newarray)
					if newarray not in avail:
						if best:
							if not winning(newarray.copy()):
								avail.append(newarray)
						else:
							avail.append(newarray)
	return avail


def nim_available(array, fast = True, best = False):
	
	avail = []
	l = len(array)

	for i in range(l):
		if array[i] == 1:
			array_copy = array.copy()
			array_copy[i] = 0
			avail.append(clean(array_copy.copy()))
			for j in range(i+1,l):
				newarray = array_copy.copy()
				newarray[j] += 1
				if fast:
					newarray[j] = newarray[j] % 2
				newarray = clean(newarray)
				if newarray not in avail:
					if best:
						if not winning(newarray.copy()):
							avail.append(newarray)
					else:
						avail.append(newarray)

	return avail


class makestate:

	def __init__(self, array, fast = True):
		self.array = clean(odd(array)) if fast else clean(array)
		self.length = len(self.array)		
		self.name = ''.join([str(i) for i in self.array]) if fast \
		else ','.join([str(i) for i in self.array])
		self.number = arrtonum(self.array) if fast else None		
		self.config = makeconfiguration(self.array)
		self.avail = available(self.array, fast, False)
		self.win = winning(self.array)
		self.fast = fast

	def bestavail(self):
		return available(self.array, self.fast, True)
		

class nim_makestate:

	def __init__(self, array, fast = True):
		self.array = clean(odd(array)) if fast else clean(array) 
		self.length = len(self.array)		
		self.name = ''.join([str(i) for i in self.array]) if fast \
		else ','.join([str(i) for i in self.array])
		self.number = arrtonum(self.array) if fast else None		
		self.config = makeconfiguration(self.array)	
		self.avail = nim_available(self.array, fast, False)
		self.win = winning(self.array)

	def bestavail(self, fast = True):
		return nim_available(self.array, fast, True)


class makeplayer:

	def __init__(self, name = 'Alice', intelligent = False, experienced = False):

		self.name = name
		self.wins = 0
		self.notebook = {}
		self.boxes = {}
		self.lastmove = None
		self.intelligent = intelligent
		self.experienced = experienced


class makegame:

	def __init__(self, level = 3, matches = 1, starter = 0, fast = True, boxsize = 42):
		self.level = level
		self.matches = matches
		self.starter = starter
		self.fast = fast
		self.start = makestate([1 for i in range(0, self.level)], self.fast)
		self.finish = makestate([0 for i in range(0, self.level)], self.fast)
		self.boxsize = boxsize


def match(player, other, game):
	
	state = game.start
	
	while state.name != game.finish.name:

		if state.name not in player.boxes:
			player.boxes[state.name] = [game.boxsize for i in state.avail]
			other.boxes[state.name] = [game.boxsize for i in state.avail]

		oldstate = state

		array = choices(state.avail, weights = player.boxes[state.name])[0]
		
		state = makestate(array, game.fast)

		move = oldstate, state

		player.notebook[move] = player.notebook.get(move,0) + 1	
		if player.boxes[oldstate.name].count(0) != len(player.boxes[oldstate.name])-1:
			player.lastmove = move

		if state.name == game.finish.name:	
			
			winner, loser, = player, other
			winner.wins += 1
								
		player, other = other, player
		
	return winner, loser


def learn(winner, loser, learner):

	for move in winner.notebook:
		color = move[0].avail.index(move[1].array)
		change = learner.boxes[move[0].name][color] + 1
		learner.boxes[move[0].name][color] = change

	for move in loser.notebook:
		color = move[0].avail.index(move[1].array)	
		change = learner.boxes[move[0].name][color] - 1	
		if change > 0:
			learner.boxes[move[0].name][color] = change
				

def prune(loser, learner):

	last = loser.lastmove
	color = last[0].avail.index(last[1].array)
	box = learner.boxes[last[0].name]
	
	for i in range(len(box)):
		if i == color:
			box[i] = 0
		else:
			box[i] += 1


def play(alice, bob, game):

	if alice.experienced:
		pass
		
	if bob.experienced:
		pass

	for i in range(game.matches):

		if game.starter == 0:
			player = choices([alice,bob])[0]
		elif game.starter == 1:
			player = alice
		elif game.starter == 2:
			player = bob
		else: break
		
		alice.notebook, bob.notebook = {}, {}
		other = bob if (player == alice) else alice
		winner, loser = match(player, other, game)
		
		if alice.intelligent:
			learn(winner, loser, alice)
			# prune(loser, alice)
			
		if bob.intelligent and bob.boxes != alice.boxes:
			learn(winner, loser, bob)
			# prune(loser, bob)
			

def goodmoves(player, state, boxsize = 42):

	goodmoves = []
	beads = player.boxes.get(state.name, None)
	
	if beads != None:
		for color in range(len(beads)):
			if beads[color] > boxsize:		
				move = state.avail[color]
				goodmoves.append(move)
	
	return goodmoves
			



