print('-------------------------------------------')
print('     COUNTING WINNING AND LOSING MOVES     ')
print('-------------------------------------------')


import random

LEVEL = 5
LEVEL += 1
GAMES = 1

WINNING, LOSING = {}, {}

for g in range(GAMES):

	STRATEGY_ALICE, STRATEGY_BOB = {}, {}

	STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
	COUNTS = [STATES.count([i for i in range(j)]) for j in range(1,LEVEL)]
	MEGA_S = [COUNTS]

	PLAYER = 0
			
	print(STATES)
	print(COUNTS)
	print(MEGA_S)		
			
	while STATES !=[]:
			
		POSSIB = [sum([i for i in range(len(j)+1)])  for j in STATES]
		
		print(POSSIB)
		
		R_LINE = random.choices(STATES, weights=POSSIB)[0]		
		X = random.choices(R_LINE, weights=[1 for i in range(len(R_LINE))])[0]
		WEIG_Y = []
		for i in range(len(R_LINE)):
			if i != X: WEIG_Y.append(1)
			else: WEIG_Y.append(2)
		Y = random.choices(R_LINE, weights=WEIG_Y)[0]
		
		if X > Y: X,Y = Y,X
		L_SIDE = [i for i in range(len(R_LINE[:X]))]
		R_SIDE = [i for i in range(len(R_LINE[Y+1:]))]
		STATES.remove(R_LINE)
		if L_SIDE != []:
			STATES.append(L_SIDE)
		if R_SIDE != []:
			STATES.append(R_SIDE)	
		
		OLDCOUNTS = COUNTS
		COUNTS = [STATES.count([i for i in range(j)]) for j in range(1,LEVEL)]
		MEGA_S.append(COUNTS)
		
		MOVE = ','.join(map(str, OLDCOUNTS))+' > '+','.join(map(str, COUNTS))
		
		if PLAYER == 0:
			if MOVE not in STRATEGY_ALICE:
				STRATEGY_ALICE[MOVE] = 1
			else:
				STRATEGY_ALICE[MOVE] += 1
		else:
			if MOVE not in STRATEGY_BOB:
				STRATEGY_BOB[MOVE] = 1
			else:
				STRATEGY_BOB[MOVE] += 1
		PLAYER += 1
		PLAYER = PLAYER % 2

### Define winning and losing moves
								
	for i in STRATEGY_ALICE:
		if PLAYER == 0:
			WINNING[i] = WINNING.get(i,0) + STRATEGY_ALICE[i]
		else:
			LOSING[i] = LOSING.get(i,0) + STRATEGY_ALICE[i]
	for i in STRATEGY_BOB:
		if PLAYER == 1:
			WINNING[i] = WINNING.get(i,0)+STRATEGY_BOB[i]
		else:
			LOSING[i] = LOSING.get(i,0)+STRATEGY_BOB[i]

### Print winning and losing moves:

print("Winning moves:")

for i in sorted(WINNING):
	print(i,'   :   ',WINNING[i])
	
print("Losing moves:")

for i in sorted(LOSING):
	print(i,'   :   ',LOSING[i])


