from module import nimplus as np
from math import log
import matplotlib.pyplot as plt


def update(state):

	newbestmoves = []
	dep_num = nimplus.arrtonum(state.array)

	for i in state.avail:	
		arr_num = nimplus.arrtonum(i)
		if not nimplus.winning(i):
			newbestmoves.append([dep_num,arr_num])			

	return newbestmoves
	

level = 7
bestmoves = []

for k in range(1, 2 ** level):

	array = np.numtoarr(k)
	state = np.makestate(array)

	newbestmoves = update(state)
	
	bestmoves.extend(newbestmoves)
	bestmovesplus.extend(newbestmovesplus)
	dictionary[k] = dictionary.get(k,'L')
	dictionaryplus[k] = dictionaryplus.get(k,'L')

both_x = []
both_y = []
nim_x = []
nim_y = []
plus_x = []
plus_y = []

for k in bestmoves:
	if k in bestmovesplus:
		both_x.append(k[0])
		both_y.append(k[1])
	else:
		nim_x.append(k[0])
		nim_y.append(k[1])

for k in bestmovesplus:
	if k not in bestmoves:
		plus_x.append(k[0])
		plus_y.append(k[1])
			



s = .2
fig = plt.figure(figsize = (6,6)) # default is (8,6)
ax = fig.add_subplot(111, aspect = 'equal')
ax.scatter(both_x, both_y, s, color = "red", alpha = 1)
# ax.scatter(nim_x, nim_y, s, color = "black", alpha = 1)
ax.scatter(plus_x, plus_y, s, color = "black", alpha = 1)
ax.set_xlim((0,2 ** level))
ax.set_ylim((0,2 ** level))
plt.axis('off')
plt.show()

 










