from module import nimplus

import matplotlib.pyplot as plt 

def name(array):
	return ''.join([str(i) for i in array])

def moves(k):

	array = [int(i) for i in bin(k)[2:]]

	state = nimplus.makestate(array, True)

	newmoves = [[],[]]

	for i in state.avail:

		newmoves[0].append(int(name(array),2))
		newmoves[1].append(int(name(i),2))
			
	return newmoves


level = 7

x = []
y = []

for k in range(1,2 ** level):
	newmoves = moves(k)
	x.extend(newmoves[0])
	y.extend(newmoves[1])

s = 2
fig = plt.figure(figsize = (6,6))
ax = fig.add_subplot(111, aspect = 'equal')
ax.scatter(x, y, s, color = "black", alpha = 1) 
ax.set_xlim((0,2 ** level))
ax.set_ylim((0,2 ** level))
plt.axis('off')
plt.show()






