

alice = bob = {
		'A' : [0,15,15,15,0],
		'B' : [0,0,0,15,0],
		'C' : [0,0,0,15,15],
		'D' : [0,0,0,0,15],
		'E' : [0,0,0,0]
		
		}

wins = 0

for i in range(100):

		moves = { 0 : {} ,
        	          1 : {}
        	        }

		state = 'A'

		player = random.choices([0,1])[0]
	
		while state != 'E':
	
			player = (player + 1) % 2
	
			oldstate = state

			state = random.choices(['A','B','C','D','E'], weights = weights[player][state])[0]
		
			moves[player][oldstate+' '+state] = moves[player].get(oldstate+' '+state,0) + 1

		if player == 0:

			wins += 1
		
		for i in moves[player]:
	
			X, Y = i.split()[0], i.split()[1]

			weights[0][X][ord(Y)-65] += 1
	
		for i in moves[(player+1) % 2]:
	
			X, Y = i.split()[0], i.split()[1]

			if weights[0][X][ord(Y)-65] != 0:
		
				weights[0][X][ord(Y)-65] -= 1

	print('Alice wins', round(wins/games*100,1), '% of times')
	


