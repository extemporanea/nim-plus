import random, copy
from math import log

class persona:
	def __init__(self, name):
		self.name = name
		self.boxes = copy.deepcopy(boxes)				
		self.note = note()
		
class note:
	def __init__(self):
		self.wins = 0
		self.winning = {}
		self.losing = {}
		self.page = {}
		
def match(player):
	
	alice.note.page, bob.note.page = {}, {}		
	state = 'A'	

	while state != 'E':
	
		other = bob if (player == alice) else alice
		move, state = action(state,player)
		player.note.page[move] = player.note.page.get(move,0) + 1
		if state == 'E': record(player,other)
		player = other

def action(state,player):

	oldstate = state
	state = random.choices(configs, weights = player.boxes[state])[0]
	move = oldstate,state
	return(move,state)

def record(player,other):

	player.wins += 1
	for move in player.page:
		player.winning[move] = player.winning.get(move,0) + 1
		other.winning[move] = other.winning.get(move,0) + 1
	for k in other.page:
		player.losing[move] = player.losing.get(move,0) + 1
		other.losing[move] = other.losing.get(move,0) + 1

def learn(player):

	for move in player.winning:
		k = player.boxes[move[0]][ord(move[1])-65] + player.winning[move]*pace
		if k < capacity:
			player.boxes[move[0]][ord(move[1])-65] = k			
	for move in player.losing:
		k = player.boxes[move[0]][ord(move[1])-65] - player.losing[move]*pace
		if k > 0:
			player.boxes[move[0]][ord(move[1])-65] = k
			
	player.losing = {}
	player.winning = {}

def game():

	if starter == 'alice': player = alice
	elif starter == 'bob': player = bob

	for i in range(matches):
	
		if starter == 'coin': player = random.choices([alice,bob])[0]
		
		match(player)
		
		if learner == 'alice': learn(alice)
		elif learner == 'bob': learn(bob)
		elif learner == 'both': learn(alice), learn(bob)
		else: pass

configs = 'A','B','C','D','E'


beads = 50
capacity = 100
level = 2
boxes = {'A' : [0,beads,beads,beads,0],
				'B' : [0,0,0,beads,0],
				'C' : [0,0,0,beads,beads],
				'D' : [0,0,0,0,beads],
				'E' : [0,0,0,0]
				}

alice, bob = persona('alice'), persona('bob')

starter = input("Who starts? (alice/bob/coin) ")
learner = input("Who learns? (alice/bob/both/none) ")
pace = log(int(input("How fast (0-10)? "))+1)/log(11)
matches = int(input("How many matches? "))

game()

print("Alice wins ", alice.note.wins, " times.")










