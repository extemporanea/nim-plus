print('-----------------------------')
print('     ALICE LEARNS TO WIN     ')
print('        SECOND LEVEL         ')
print('-----------------------------')


import random

GAMES = 1000

STONES = 100

WEIGHTS = { 0 : {'A' : [0,STONES,STONES,STONES,0],
                       'B' : [0,0,0,STONES,0],
                       'C' : [0,0,0,STONES,STONES],
                       'D' : [0,0,0,0,STONES],
                       'E' : [0,0,0,0]},
            1 : {'A' : [0,STONES,STONES,STONES,0],
                       'B' : [0,0,0,STONES,0],
                       'C' : [0,0,0,STONES,STONES],
                       'D' : [0,0,0,0,STONES],
                       'E' : [0,0,0,0]}
          }

WINS = 0
	

for i in range(GAMES):

	MOVES = { 0 : {} ,
                  1 : {}
                }

	STATE = 'A'

	PLAYER = 1
	
	# Alice will start
	
	while STATE != 'E':
	
		PLAYER = (PLAYER + 1) % 2
	
		OLDSTATE = STATE

		STATE = random.choices(['A','B','C','D','E'], weights = WEIGHTS[PLAYER][STATE])[0]
		
		MOVES[PLAYER][OLDSTATE+' '+STATE] = MOVES[PLAYER].get(OLDSTATE+' '+STATE,0) + 1

		
	print(MOVES[0])
	print(MOVES[1])
	
	if PLAYER == 0:

		WINS += 1
		
	for i in MOVES[PLAYER]:
	
		X, Y = i.split()[0], i.split()[1]
		
		if WEIGHTS[0][X][ord(Y)-65] != 300:
		
			WEIGHTS[0][X][ord(Y)-65] += 1
	
	for i in MOVES[(PLAYER+1) % 2]:
	
		X, Y = i.split()[0], i.split()[1]

		if WEIGHTS[0][X][ord(Y)-65] != 0:
		
			WEIGHTS[0][X][ord(Y)-65] -= 1

print('Alice wins:', round(WINS/GAMES*100,1), '% of times')

# This makes perfect sense, but we have to remember is per game and not per total moves!	




