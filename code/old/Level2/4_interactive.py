import random

REPRESENTATION = {
  0: "\n0:    | \n     | | \n" ,
  1: "\n1:    | \n     + | \n",
  2: "\n2:    + \n     | | \n",
  3: "\n3:    + \n     + | \n",
  4: "\n4:    + \n     + + \n"
  }

STONES = 5

URNS = [[0,STONES,STONES,STONES,0],[0,0,0,STONES,0],[0,0,0,STONES,STONES],[0,0,0,0,STONES],[0,0,0,0]]

AGAIN = 1

print('\n\n')

while AGAIN == 1:

	MOVES = { 0 : {} , 1 : {} }

	STATE = 0

	PLAYER = random.choices([0,1], weights = [1,1])[0]
	
	while STATE != 4:

		PLAYER = (PLAYER + 1) % 2
	
		OLDSTATE = STATE
			
		if PLAYER == 1:
		
			STATE = random.choices([0,1,2,3,4], weights = URNS[STATE])[0]
						
			print('--------------------------------------------------------\n')
			print('Tocca a me! Scelgo\n', REPRESENTATION[STATE], '\n\n')
			
		else:
			
			OPTIONS = []
		
			for i in [0,1,2,3,4]: 
				
				if URNS[STATE][i] != 0:
				
					OPTIONS.append(i)					
		
			print('--------------------------------------------------------\n')
			print('Scegli che mossa fare tra queste (scrivi il numero e schiaccia INVIO):')
			
			for i in OPTIONS:
			
				print(REPRESENTATION[i]) 
			
			STATE = int(input())

			while STATE not in OPTIONS:

				print(random.choices(['Zuzzurellon\u0259!',
				'Malandrin\u0259!',
				'Borghes\u0259!'], weights = [1,1,1])[0])
				
				print("Guarda l'elenco e scegli un numero tra questi:", 
				OPTIONS)			

				STATE = int(input())	

		MOVES[PLAYER][str(OLDSTATE)+' '+str(STATE)] = MOVES[PLAYER].get(str(OLDSTATE)+' '+str(STATE),0) + 1

	if PLAYER == 0: print('Hai vinto!\n')
	
	else: print('Hai perso!\n')

	for i in MOVES[PLAYER]:
	
		X, Y = int(i.split()[0]), int(i.split()[1])

		URNS[X][Y] += 1
	
	for i in MOVES[(PLAYER+1) % 2]:
	
		X, Y = int(i.split()[0]), int(i.split()[1])

		if URNS[X][Y] != 1:
		
			URNS[X][Y] -= 1
	
	AGAIN = int(input('Vuoi giocare ancora? (0 = no, 1 = si) + INVIO\n'))
	

