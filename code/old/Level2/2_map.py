print('-----------------------------')
print('     ALICE LEARNS TO WIN     ')
print('        SECOND LEVEL         ')
print('-----------------------------')


import random


GAMES = 1000000

STONES = 100

WEIGHTS = { 'A' : [0,STONES,STONES,STONES,0],
            'B' : [0,0,0,STONES,0],
            'C' : [0,0,0,STONES,STONES],
            'D' : [0,0,0,0,STONES],
            'E' : [0,0,0,0]}

WINS = 0
	
MOVES = { }

for i in range(GAMES):

	STATE = 'A'

	PLAYER = 'Alice'
	
	while STATE != 'E':
	
		OLDSTATE = STATE

		STATE = random.choices(['A','B','C','D','E'], weights = WEIGHTS[STATE])[0]
		
		MOVES[OLDSTATE+STATE] = MOVES.get(OLDSTATE+STATE,0) + 1

		if PLAYER == 'Alice':
			PLAYER = 'Bob'
		else:
			PLAYER = 'Alice'
	
	if PLAYER == 'Alice':
	
		WINS += 1

print('How often Alice wins:', round(WINS/GAMES*100,1), '%')

print('How often moves that have been played (in each round):')

for i in MOVES:
	print(i, ' : ', round(MOVES[i]/GAMES*100,1), '%')

# This makes perfect sense, but we have to remember is per game and not per total moves!	




