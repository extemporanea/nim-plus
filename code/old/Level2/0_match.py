import random

beads = 10

configs = 'A','B','C','D','E'

boxes = {
	'A' : [0,beads,beads,beads,0],
	'B' : [0,0,0,beads,0],
	'C' : [0,0,0,beads,beads],
	'D' : [0,0,0,0,beads],
	'E' : [0,0,0,0]	
	}

class persona:
	def __init__(self, name):
		self.name = name
		self.boxes = boxes

def match():
	state = 'A'
	player = starter = random.choices([alice,bob])[0]
	while state != 'E':
		state = random.choices(configs, weights = player.boxes[state])[0]
		player = bob if (player == alice) else alice
	return(starter.name, player.name)

alice, bob = persona("Alice"), persona("Bob")

starter, winner = match()

print(starter, 'starts,', winner, 'wins.')
	


