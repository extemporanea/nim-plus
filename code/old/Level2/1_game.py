import random, copy

class persona:
	def __init__(self, name):
		self.name = name
		self.boxes = copy.deepcopy(boxes)				
		self.note = note()
		
class note:
	def __init__(self):
		self.wins = 0
		self.winning = {}
		self.losing = {}
		self.page = {}
		
def match(player):
	
	alice.note.page, bob.note.page = {}, {}		
	state = 'A'	

	while state != 'E':
	
		other = bob if (player == alice) else alice
		oldstate = state
		state = random.choices(configs, weights = player.boxes[state])[0]
		move = oldstate,state
		player.note.page[move] = player.note.page.get(move,0) + 1
		if state == 'E': record(player,other)
		player = other
		
def record(player,other):

	player.note.wins += 1
	for k in player.note.page:
		player.note.winning[k] = player.note.winning.get(k,0) + 1
		other.note.winning[k] = other.note.winning.get(k,0) + 1
	for k in other.note.page:
		player.note.losing[k] = player.note.losing.get(k,0) + 1
		other.note.losing[k] = other.note.losing.get(k,0) + 1

def learn(player):

	for move in player.note.winning:	
		if player.boxes[move[0]][ord(move[1])-65] < capacity:
			player.boxes[move[0]][ord(move[1])-65] += player.note.winning[move]
					
	for move in player.note.losing:
		if player.boxes[move[0]][ord(move[1])-65] - player.note.losing[move] > 1:
			player.boxes[move[0]][ord(move[1])-65] -= player.note.losing[move]
			
	player.note.losing = {}
	player.note.winning = {}	


def game(level, beads, matches, starter, learner):

	if starter == 'alice': player = alice
	elif starter == 'bob': player = bob

	for i in range(matches):
	
		if starter == 'coin': player = random.choices([alice,bob])[0]
		
		match(player)
		
		if learner == 'alice': learn(alice)
		elif learner == 'bob': learn(bob)
		elif learner == 'both':
			learn(alice)
			learn(bob)
		else: pass

configs = 'A','B','C','D','E'


beads = 20
capacity = 200
level = 2
boxes = {'A' : [0,beads,beads,beads,0],
				'B' : [0,0,0,beads,0],
				'C' : [0,0,0,beads,beads],
				'D' : [0,0,0,0,beads],
				'E' : [0,0,0,0]
				}

alice, bob = persona('alice'), persona('bob')

starter = input("Who starts? (alice/bob/coin) ")
learner = input("Who learns? (alice/bob/both/none) ")
matches = int(input("How many matches? "))

game(level,beads,matches,starter,learner)

print("Alice wins ", alice.note.wins, " times.")










