from random import choices
from math import log, floor


def clean(array):

	for i in range(len(array)):
		if array[0] == 0 and len(array) != 1:
			array.pop(0)
				
	return array
		

def odd(array):

	for i in range(len(array)):
		array[i] %= 2
		
	return array
	
	
def arrtonum(array):

	array = odd(array)
	
	return int(''.join([str(i) for i in array]), 2)
	

def numtoarr(number):

	return [int(i) for i in bin(number)[2:]]
	
def arrtoname(array, fast = True):

	if fast:
		return ''.join([str(i) for i in array])
	else:
		 return ','.join([str(i) for i in array])

def nametoarr(name, fast = True):

	if fast:
		return [int(i) for i in name]
	else:
		return [int(i) for i in name.split(",")]
	
	
def arrtoconfig(array):

	config = []	
	
	for i in range(len(array)):
		for j in range(array[-i-1]):
			config.append([k for k in range(i+1)])

	return config


def configtoarr(config):

	l = 0
	
	for i in config:
		if len(i) > l:
			l = len(i)
		
	array = [config.count([i for i in range(j)]) for j in range(1, l+1)]
	array.reverse()
	array = clean(array)

	return array	
	

def winning(array):

	array = clean(odd(array))
	l = len(array)
	binaries = []
	digits = floor(log(len(array), 2)) + 1

	for i in range(l):
		if array[i] != 0:
			value = [int(i) for i in bin(l-i)[2:]]
			for k in range(len(value), digits):
				value = [0] + value		
			binaries.append(value)

	nimsum = [0 for i in range(digits)]

	for k in range(digits):
		for i in range(len(binaries)):
			nimsum[k] += binaries[i][k]
		nimsum[k] = nimsum[k] % 2

	return False if clean(nimsum) == [0] else True



def include(newarray, available, fast, best):
			
	if (best and not winning(newarray.copy())) or not best:
		if fast:
			odd(newarray)
			clean(newarray)
			if newarray not in available:
				available.append(newarray)
		else:
			clean(newarray)
			available.append(newarray)
	else:
		pass					
		

def available(array, fast = True, plus = True, best = False):

	available = []

	if plus:

		l = len(array)

		for i in range(l):
			if array[i] != 0:
				for k1 in range(l-i+1):
					for k2 in range(k1,l-i-k1):
						newarray = array.copy()
						newarray[i] -= 1
						if k1 != 0:
							newarray[l-k1] += 1
						if k2 != 0:
							newarray[l-k2] += 1
							
						include(newarray, available, fast, best)	
					
	else:
	
		l = len(array)

		for i in range(l):
			if array[i] == 1:
				array_copy = array.copy()
				array_copy[i] = 0
				avail.append(clean(array_copy.copy()))
				for j in range(i+1,l):
					newarray = array_copy.copy()
					newarray[j] += 1
					if fast:
						newarray[j] = newarray[j] % 2
					newarray = clean(newarray)			
					
					include(newarray, available, fast, best)	
	

	return available


class makestate:

	def __init__(self, array, fast = True, plus = True):
		self.fast = fast
		self.plus = plus
		self.array = clean(odd(array)) if self.fast else clean(array)
		self.length = len(self.array)		
		self.name = arrtoname(self.array.copy(), fast)
		self.number = arrtonum(self.array.copy()) if self.fast else None		
		self.win = winning(self.array.copy())

	def set_avail(self):
		self.avail = available(self.array, self.fast, self.plus, False)

	def set_bestavail(self):
		self.bestavail = available(self.array, self.fast, self.plus, True)


class makeplayer:

	def __init__(self, intelligent = False, smart = False, experienced = False):
		self.wins = 0
		self.notebook = {}
		self.boxes = {}
		self.lastmove = None
		self.intelligent = intelligent
		self.smart = smart
		self.experienced = experienced
		
	def learns(self, winner, loser):
		if self.intelligent: reinforce(self, winner, loser)
		if self.smart: prune(self, loser)		


class makegame:

	def __init__(self, level = 3, matches = 1, starter = 0, fast = True, boxsize = 42):
		self.level = level
		self.start = makestate([1 for i in range(0, self.level)])
		self.matches = matches
		self.starter = starter
		self.fast = fast
		self.boxsize = boxsize


def match(player, other, game):
	
	state = game.start
	
	while state.name != '0':

		state.set_avail()		

		if state.name not in player.boxes:
			player.boxes[state.name] = [game.boxsize for i in state.avail]
			
			if other.boxes != player.boxes:
				other.boxes[state.name] = [game.boxsize for i in state.avail]

		oldstate = state
		array = choices(state.avail, weights = player.boxes[state.name])[0]
		state = makestate(array, game.fast)
		move = oldstate, state
		player.notebook[move] = player.notebook.get(move,0) + 1

		if player.boxes[oldstate.name].count(0) != len(player.boxes[oldstate.name])-1:
			player.lastmove = move

		if state.name == '0':	
			winner, loser, = player, other
			winner.wins += 1
								
		player, other = other, player
		
	return winner, loser


def reinforce(learner, winner, loser):

	for move in winner.notebook:

		move[0].set_avail()
		color = move[0].avail.index(move[1].array)
		change = learner.boxes[move[0].name][color] + 1
		learner.boxes[move[0].name][color] = change

	for move in loser.notebook:
		move[0].set_avail()
		color = move[0].avail.index(move[1].array)	
		change = learner.boxes[move[0].name][color] - 1	
		if change > 0:
			learner.boxes[move[0].name][color] = change
				

def prune(learner, loser):

	last = loser.lastmove
	last[0].set_avail()
	color = last[0].avail.index(last[1].array)
	box = learner.boxes[last[0].name]
	
	for i in range(len(box)):
		if i == color:
			box[i] = 0
		else:
			box[i] += 1


def goodmoves(player, state, boxsize = 42):

	goodmoves = []
	beads = player.boxes.get(state.name, None)
	
	if beads != None:
		for color in range(len(beads)):
			if beads[color] > boxsize:		
				move = state.avail[color]
				goodmoves.append(move)
	
	return goodmoves


def play(alice, bob, game):

	if alice.experienced:
		pass
		
	if bob.experienced:
		pass

	for i in range(game.matches):

		if game.starter == 0:
			player = choices([alice,bob])[0]
		elif game.starter == 1:
			player = alice
		elif game.starter == 2:
			player = bob
		else: break
		
		other = bob if (player == alice) else alice
		winner, loser = match(player, other, game)
		
		alice.learns(winner, loser)
		bob.learns(winner, loser)
		alice.notebook = {}
		bob.notebook = {}
			

			



