class makeplayer:

	def __init__(caya):
		caya.brainbeads = 0
		caya.colorbeads = 0
		caya.wins = 0
		caya.boxes = {}
		caya.page = {}
		
class makestate:

	def __init__(caya, array):
		caya.array = array
		caya.level = len(array)
		caya.config = configuration(caya)
		caya.name = ','.join([str(i) for i in caya.array])
		caya.avail = available(caya)
		
def match(player,other,start,finish):
	
	player.page, other.page = {}, {}
	state = start
	
	while state.name != finish.name:
		
		if state.name not in player.boxes:

			player.boxes[state.name] = [player.colorbeads for i in state.avail]
			other.boxes[state.name] = [other.colorbeads for i in state.avail]

		oldstate = state	
		state = makestate(random.choices(state.avail, weights = player.boxes[state.name])[0])

		move = oldstate, state
		player.page[move] = player.page.get(move,0) + 1

		if state.name == finish.name:
		
			player.wins += 1
			winner, loser = player, other
			
		player, other = other, player

	return(winner, loser)

def configuration(state):

	configuration = []
	
	for i in range(state.level):
		for j in range(state.array[i]):
			configuration.append([k for k in range(i+1)])
	return(configuration)

def available(state):

	avail = []
	
	for line in state.config:
		for bar_x in line:
			for bar_y in line:
				if bar_y >= bar_x:
					configuration = copy.deepcopy(state.config)
					l_side = [i for i in range(len(line[:bar_x]))]
					r_side = [i for i in range(len(line[bar_y+1:]))]
					configuration.remove(line)
					if l_side != []: configuration.append(l_side)
					if r_side != []: configuration.append(r_side)
					array = [configuration.count([i for i in range(j)]) for j in range(1,state.level+1)]
					if array not in avail:
						avail.append(array)
	return(avail)

def learn(learner,winner,loser):

	for move in winner.page:
		color = move[0].avail.index(move[1].array)
		change = learner.boxes[move[0].name][color] + learner.brainbeads
		if change < 2 * learner.colorbeads:
			learner.boxes[move[0].name][color] = change
		
	for move in loser.page:
		color = move[0].avail.index(move[1].array)
		change = learner.boxes[move[0].name][color] - learner.brainbeads		
		if change > 0:
			 learner.boxes[move[0].name][color] = change


def game(alice,bob,coin,starter,level,matches):

	start = makestate([1 for i in range(0,level)])
	finish = makestate([0 for i in range(0,level)])

	for i in range(matches):
	
		if starter == 'c':
			player = random.choices([alice,bob])[0]
		elif starter == 'a':
			player = alice
		elif starter == 'b':
			player = bob
		else: break	
		
		other = bob if (player == alice) else alice
		winner, loser = match(player,other,start,finish)

		if alice.brainbeads != 0:
			learn(alice,winner,loser)
		if bob.brainbeads != 0:
			learn(bob,winner,loser)

def main():

	alice, bob, coin = makeplayer(), makeplayer(), makeplayer()

	alice.colorbeads = 50
	bob.colorbeads = 50

	alice.brainbeads = int(input("How smart is Alice? (0-10) ") or "0") 
	bob.brainbeads = int(input("How smart is Bob? (0-10) ") or "0") 
	
	level = int(input("Level? (1-10) ") or "3") 
	
	matches = int(input("Matches? (1-1000) ") or "1") 
	
	starter = input("Who starts? (a: Alice/b: Bob/c: Coin) ") or "a"
	
	game(alice,bob,coin,starter,level,matches)
	
	output(alice,bob,matches)


def output(alice,bob,matches):

	print("Alice wins ", alice.wins/matches*100, " % of games.")


import random, copy, math

main()

# for statename in alice.boxes: print(statename, ' : ', alice.boxes[statename])
# for statename in bob.boxes: print(statename, ' : ', bob.boxes[statename])



	










