print('-----------------------')
print('     MAPPING GAMES     ')
print('-----------------------')

import random

LEVEL = 2
LEVEL += 1
GAMES = 1

STRATEGY_ALICE, STRATEGY_BOB = {}, {}

for g in range(GAMES):

	STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
	COUNTS = [STATES.count([i for i in range(j)]) for j in range(1,LEVEL)]
	MEGA_S = [COUNTS]

	PLAYER = 0
			
	while STATES !=[]:
			
		POSSIB = [sum([i for i in range(len(j)+1)])  for j in STATES]
		
		R_LINE = random.choices(STATES, weights=POSSIB)[0]		
		X = random.choices(R_LINE, weights=[1 for i in range(len(R_LINE))])[0]
		WEIG_Y = []
		for i in range(len(R_LINE)):
			if i != X: WEIG_Y.append(1)
			else: WEIG_Y.append(2)
		Y = random.choices(R_LINE, weights=WEIG_Y)[0]
		
		if X > Y: X,Y = Y,X
		
		L_SIDE = [i for i in range(len(R_LINE[:X]))]
		R_SIDE = [i for i in range(len(R_LINE[Y+1:]))]
		STATES.remove(R_LINE)

		if L_SIDE != []:
			STATES.append(L_SIDE)
		if R_SIDE != []:
			STATES.append(R_SIDE)	
		
		OLDCOUNTS = COUNTS
		COUNTS = [STATES.count([i for i in range(j)]) for j in range(1,LEVEL)]
		MEGA_S.append(COUNTS)
		
		MOVE = ','.join(map(str, OLDCOUNTS))+' > '+','.join(map(str, COUNTS))
		
		if PLAYER == 0:
			if MOVE not in STRATEGY_ALICE:
				STRATEGY_ALICE[MOVE] = 1
			else:
				STRATEGY_ALICE[MOVE] += 1
		else:
			if MOVE not in STRATEGY_BOB:
				STRATEGY_BOB[MOVE] = 1
			else:
				STRATEGY_BOB[MOVE] += 1
		PLAYER += 1
		PLAYER = PLAYER % 2

### Print games in terms of counts:
			
	for i in range(len(MEGA_S)): print(MEGA_S[i])
	print('---------------')
	
### Print number of times a move has been performed in a game:

print("Alice's moves:")

for i in STRATEGY_ALICE:
	print(i,'   :   ',STRATEGY_ALICE[i])



print("Bob's moves:")	

for i in STRATEGY_BOB:
	print(i,'   :   ',STRATEGY_BOB[i])




