print('-----------------------------')
print('     ALICE LEARNS TO WIN     ')
print('-----------------------------')


import random
from matplotlib import pyplot as plt

LEVEL = 5
GAMES = 10
TIMES = 1

LEVEL += 1
# HISTO = []

for t in range(TIMES):

	# WALICE = 0

	for g in range(GAMES):
	
		STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
	
		PLAYER = random.choices([0, 1], weights = [1/2,1/2])[0]
	
		COUNTS = [STATES.count([i for i in range(k)]) for k in range(1,LEVEL)]
		MEGA_S = [COUNTS]

		while STATES !=[]:

			WEIGHT = [1 for j in range(len(STATES))]
			CHOICE = random.choices(STATES, weights=WEIGHT)[0]
			HEIGHT = [1 for i in range(len(CHOICE))]
			X = random.choices(CHOICE, weights=HEIGHT)[0]
			Y = random.choices(CHOICE, weights=HEIGHT)[0]
			if Y < X:
				X, Y = Y, X
			L_SIDE = [i for i in range(len(CHOICE[:X]))]
			R_SIDE = [i for i in range(len(CHOICE[Y+1:]))]
			STATES.remove(CHOICE)
			if L_SIDE != []:
				STATES.append(L_SIDE)
			if R_SIDE != []:
				STATES.append(R_SIDE)
			PLAYER = (PLAYER + 1) % 2	
			COUNTS = [STATES.count([i for i in range(k)]) for k in range(1,LEVEL)]
			MEGA_S.append(COUNTS)
			
		for i in range(len(MEGA_S)): print(MEGA_S[i])
		print('---------------')
		# if PLAYER == 1: WALICE += 1

	# HISTO.append(WALICE)
	
#plt.hist(HISTO, 2*GAMES, density=True, stacked=True)
# plt.show()
	
