from module import nimplus
from math import log
import matplotlib.pyplot as plt


def update(state):

	newbestmoves = []
	dep_num = nimplus.arrtonum(state.array)
	dictionary[dep_num] = 'L' if not nimplus.winning(state.array) else 'W'

	for i in state.avail:	
		arr_num = nimplus.arrtonum(i)
		if not nimplus.winning(i):
			newbestmoves.append([dep_num,arr_num])			

	return newbestmoves
	

def updateplus(state):

	newbestmoves = []
	dep_num = nimplus.arrtonum(state.array)

	for i in state.avail:
		arr_num = nimplus.arrtonum(i)
		if dictionaryplus.get(arr_num) == 'L':
			newbestmoves.append([dep_num,arr_num])
			dictionaryplus[dep_num] = 'W'

	return newbestmoves

level = 13
fast = True
bestmoves = []
bestmovesplus = []
dictionary = { 0 : 'L' }
dictionaryplus = { 0 : 'L' }

for k in range(1,2 ** level):

	array = nimplus.numtoarr(k)
	state = nimplus.nim_makestate(array, fast)
	stateplus = nimplus.makestate(array, fast)	
	newbestmoves = update(state)
	newbestmovesplus = updateplus(stateplus)
	bestmoves.extend(newbestmoves)
	bestmovesplus.extend(newbestmovesplus)
	dictionary[k] = dictionary.get(k,'L')
	dictionaryplus[k] = dictionaryplus.get(k,'L')

both_x = []
both_y = []
nim_x = []
nim_y = []
plus_x = []
plus_y = []

for k in bestmoves:
	if k in bestmovesplus:
		both_x.append(k[0])
		both_y.append(k[1])
	else:
		nim_x.append(k[0])
		nim_y.append(k[1])

for k in bestmovesplus:
	if k not in bestmoves:
		plus_x.append(k[0])
		plus_y.append(k[1])
			



s = .2
fig = plt.figure(figsize = (6,6)) # default is (8,6)
ax = fig.add_subplot(111, aspect = 'equal')
ax.scatter(both_x, both_y, s, color = "red", alpha = 1)
# ax.scatter(nim_x, nim_y, s, color = "black", alpha = 1)
ax.scatter(plus_x, plus_y, s, color = "black", alpha = 1)
ax.set_xlim((0,2 ** level))
ax.set_ylim((0,2 ** level))
plt.axis('off')
plt.show()

 










