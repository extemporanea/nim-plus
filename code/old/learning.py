import nimplus

alice, bob = nimplus.makeplayer(), nimplus.makeplayer()

game = nimplus.makegame()

if game.learner == 'b': bob.boxes = alice.boxes

game.fast = True if (game.fast == "Y") else False

nimplus.play(game)

print("Alice won ", alice.wins/game.matches*100, " % of matches.")

