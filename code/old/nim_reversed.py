from random import choices
from copy import deepcopy
from os import path
from ast import literal_eval

class makeplayer:

	def __init__(self, intelligent, experienced, game):
	
		self.wins = 0
		self.notebook = {}
		self.lastmove = None
		self.intelligent = intelligent
		
		filename = "strategy/fast/" if game.fast else "strategy/slow/"
		filename += str(game.level) + "_" + str(game.boxsize)

		if experienced and path.isfile(filename):
			with open(filename, "r") as file:
				self.boxes = literal_eval(file.read())
		else:
			self.boxes = {}

class makegame:

	def __init__(self, level, matches, starter, fast, boxsize):
		self.level = level
		self.matches = matches
		self.starter = starter
		self.fast = fast
		self.start = makestate([1 for i in range(0, self.level)], self.fast)
		self.finish = makestate([0 for i in range(0, self.level)], self.fast)
		self.boxsize = boxsize
	
class makestate:

	def __init__(self, array, fast):
		self.array = clean(array)
		self.length = len(array)		
		self.name = ','.join([str(i) for i in self.array])			
		self.config = configuration(self)
		self.avail = available(self, fast)

class makemove:

	def __init__(self, oldstate, state):
		self.depart = oldstate 
		self.arrive = state

def match(player, other, game):
	
	player.notebook, other.notebook = {}, {}
	state = game.start
	
	while state.name != game.finish.name:

		if state.name not in player.boxes:
			player.boxes[state.name] = [game.boxsize for i in state.avail]
			other.boxes[state.name] = [game.boxsize for i in state.avail]

		oldstate = state
		array = choices(state.avail, weights = player.boxes[state.name])[0]
		state = makestate(array, game.fast) 

		move = makemove(oldstate, state)
		player.notebook[move] = player.notebook.get(move,0) + 1
		
		if player.boxes[oldstate.name].count(0) != len(player.boxes[oldstate.name])-1:
			player.lastmove = move

		if state.name == game.finish.name:		
			winner, loser, = player, other
			winner.wins += 1
								
		player, other = other, player
		
	return winner, loser

def clean(array):

	for i in range(len(array)):
		if array[-1] == 0:
			array.pop(-1)
	return array	

def even(array):

	for i in range(len(array)):
		array[i] = array[i] % 2
	return array
	
def configuration(state):

	configuration = []
	
	for i in range(state.length):
		for j in range(state.array[i]):
			configuration.append([k for k in range(i+1)])
			
	return(configuration)

def available(state, fast):

	avail = []
	
	for line in state.config:
		for bar_x in line:
			for bar_y in line:
				if bar_y >= bar_x:
					configuration = deepcopy(state.config)
					l_side = [i for i in range(len(line[:bar_x]))]
					r_side = [i for i in range(len(line[bar_y+1:]))]
					configuration.remove(line)
					if l_side != []:
						configuration.append(l_side)
					if r_side != []:
						configuration.append(r_side)
					array = [configuration.count([i for i in range(j)]) \
						for j in range(1,state.length+1)]
					if fast:
						array = even(array) 
					array =  clean(array)
					if array not in avail:
						avail.append(array)
	return(avail)

def learn(winner, loser, learner):

	for move in winner.notebook:
		color = move.depart.avail.index(move.arrive.array)
		change = learner.boxes[move.depart.name][color] + 1
		learner.boxes[move.depart.name][color] = change

	for move in loser.notebook:
		color = move.depart.avail.index(move.arrive.array)	
		if move == loser.lastmove:
			learner.boxes[loser.lastmove.depart.name][color] = 0
		else:
			change = learner.boxes[move.depart.name][color] - 1	
			if change > 0:
				learner.boxes[move.depart.name][color] = change


def play(game, alice, bob):

	for i in range(game.matches):

		if game.starter == 0:
			player = choices([alice,bob])[0]
		elif game.starter == 1:
			player = alice
		elif game.starter == 2:
			player = bob
		else: break
		
		other = bob if (player == alice) else alice
		
		winner, loser = match(player, other, game)
		
		if alice.intelligent:
			learn(winner, loser, alice)
			
		if bob.intelligent and bob.boxes != alice.boxes:
			learn(winner, loser, bob)
			




