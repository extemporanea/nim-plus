print('--------------------------')
print('   HOW OFTEN ALICE WINS   ')
print('     WHEN SHE STARTS      ')
print('--------------------------')

### THE POINT HERE IS THAT ALICE DOES NOT WIN AS OFTEN AS WE WOULD EXPECT, BECAUSE WE ARE NOT CODING CORRECTLY EVENTUALITIES!


import random

LEVEL = 5
GAMES = 100000
WALICE = 0

LEVEL += 1

for g in range(GAMES):
	
	STATES = [[i for i in range(j)] for j in range(1,LEVEL)]
	
#	if g == 0: print(STATES)
	
	k = 0
	
	while STATES !=[]:

		R_LINE = random.choices(STATES, weights=[1 for j in range(len(STATES))])[0]
		X = random.choices(R_LINE, weights=[1 for i in range(len(R_LINE))])[0]
		Y = random.choices(R_LINE, weights=[1 for i in range(len(R_LINE))])[0]
		
		if X < Y: X,Y = Y,X
		L_SIDE = [i for i in range(len(R_LINE[:X]))]
		R_SIDE = [i for i in range(len(R_LINE[Y+1:]))]
		STATES.remove(R_LINE)
		if L_SIDE != []:
			STATES.append(L_SIDE)
		if R_SIDE != []:
			STATES.append(R_SIDE)
		k += 1
		k = k % 2
		
	if k == 1: WALICE += 1
	
print('Alice won', WALICE/GAMES*100,'% of games')

