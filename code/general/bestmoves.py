import nimplus as np
import matplotlib.pyplot as plt

level = 7
offset = .5

best_x, best_y = [], []
rest_x, rest_y = [], []

for k in range(0, 2 ** level):
	
	state = np.State(k, plus = True)
	state.set_avail()

	for newstate in state.avail:
	
		j = newstate.number
	
		if not newstate.winning():
			best_x.append(k+offset)
			best_y.append(j+offset)
		else:
			rest_x.append(k+offset)
			rest_y.append(j+offset)
			

	
s = 10
fig = plt.figure(figsize = (6,6))
ax = fig.add_subplot(111, aspect = 'equal')
ax.scatter(best_x, best_y, s, color = 'green', alpha = 1)
ax.scatter(rest_x, rest_y, s, color = 'grey', alpha = .5)
ax.set_xlim((0,2 ** level))
ax.set_ylim((0,2 ** level))
plt.axis('off')
plt.show()

 









