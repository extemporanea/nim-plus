import nimplus as np
import matplotlib.pyplot as plt


def makeframe(alice, level, t):

	poss_moves = []
	best_moves = []
	good_moves = []

	for k in range(1, 2 ** level):

		state = np.State(k)
		state.set_avail(False)
		for i in state.avail:
			poss_moves.append([k,i.number])
		state.set_avail(True)
		for i in state.avail:
			best_moves.append([k,i.number])
		for i in alice.goodmoves(state):
			good_moves.append([k,i.number])

	goodx, goody = [], []
	
	for k in good_moves:
		if k in best_moves:
			goodx.append(k[0]+.5)
			goody.append(k[1]+.5)
	
	badx, bady = [], []

	for k in good_moves:
		if k not in best_moves:
			badx.append(k[0]+.5)
			bady.append(k[1]+.5)

	bestx, besty = [], []

	for k in best_moves:
		if k not in good_moves:
			bestx.append(k[0]+.5)
			besty.append(k[1]+.5)

	allx, ally = [], []
	gray = 0
	for k in poss_moves:
		gray += 1
		if k not in best_moves:
			if k not in good_moves:
				allx.append(k[0]+.5)
				ally.append(k[1]+.5)
	
	s = 5
	fig = plt.figure(figsize = (6,6))
	ax = fig.add_subplot(111, aspect = 'equal')
	ax.scatter(goodx, goody, s, color = "green", alpha = 1)
	ax.scatter(badx, bady, s, color = "red", alpha = 1)
	ax.scatter(bestx, besty, s, color = "yellow", alpha = 1)
	ax.scatter(allx, ally, s, color = "gray", alpha = 1)
	ax.set_xlim((0,2 ** level))
	ax.set_ylim((0,2 ** level))
	plt.axis('off')
	plt.savefig('run/'+str(t)+'.png')

level = 7
intervals = [i*2 for i in range(10)]

alice = np.Player(True)
bob = np.Player(True)

t = 0

for i in intervals:
	t += i
	game = np.Game(level, t)
	game.play(alice, bob)
	makeframe(alice, level, t)



