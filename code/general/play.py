import nimplus

'''Plays a random game between Alice and Bob, returns percentages of wins.'''

level = int(input("Level? [Default: 3] ") or "3")
matches = int(input("Matches? [Default: 10000] ") or "10000") 
starter = int(input("Who starts? (0: random / 1: alice / 2: bob) [Default: 0] ") or "0")
learner = int(input("Who learns? (0: none / 1: alice / 2: bob / 3: both) [Default: 0]  ") or "0")

if learner == 3:
	reinforces = True, True
elif learner == 2:
	reinforces = False, True
elif learner == 1:
	reinforces = True, False
else:
	reinforces = False, False

alice = nimplus.Player(trims = reinforces[0])
bob = nimplus.Player(trims = reinforces[1])

if starter == 0:
	starter = None
elif starter == 1:
	starter = alice
elif starter == 2:
	starter = bob

game = nimplus.Game(level, matches, starter)

game.play(alice, bob)

print("Alice won ", alice.wins/game.matches*100, " % of matches.")
print("Bob won ", bob.wins/game.matches*100, " % of matches.")
