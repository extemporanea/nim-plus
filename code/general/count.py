import nimplus

class Counter:

	def __init__(self):
		self.games = 0
		self.turn = 0
		self.wins = 0
		self.moves = []

def recursive(state):

	if state == finish:
		counter.games += 1
		counter.moves.append(counter.turn)
		if counter.turn % 2 == 1:
			counter.wins += 1
	else:
		state.set_avail()
		counter.turn += 1
		for newstate in state.avail:	
			recursive(newstate)				
		counter.turn -= 1

level = int(input("Level? [Default: 3] ") or "3")

counter = Counter()
start = nimplus.State([1 for i in range(0, level)], plus = True)
finish = nimplus.finish

recursive(start)

print('Number of games: ', counter.games)
print('Number of wins: ', counter.wins)
print('Average number of moves: ', sum(counter.moves)/len(counter.moves))



