from nimplus import Player, Game
import matplotlib.pyplot as plt

level = 5

alice = Player(trims = True)
bob = Player(prunes = True)
carl = Player(reinforces = True, beads = 10)
dorothy = Player()

game = Game(level, 1)

colors = [[],[],[],[]]
runs = 3000

for t in range(runs):

	game.play(alice, dorothy)
	game.play(bob, dorothy)
	game.play(carl, dorothy)

	blue = 0
	green = 0
	yellow = 0
	red = 0

	for i in alice.boxes:
		if alice.boxes[i] > alice.beads:
			if not i[1].winning():
				green += 1

	for i in bob.boxes:
		if bob.boxes[i] > bob.beads:
			if not i[1].winning():
				blue += 1
				
	for i in carl.boxes:
		if carl.boxes[i] > carl.beads:
			if i[1].winning():
				red += 1
			else:
				yellow += 1

	colors[0].append(green)
	colors[1].append(blue)
	colors[2].append(yellow)
	colors[3].append(red)

x = [t for t in range(0, runs)]

plt.plot(x, colors[0], color = "green")
plt.plot(x, colors[1] , color = "blue") 
plt.plot(x, colors[2], color = "yellow")
plt.plot(x, colors[3] , color = "red") 
plt.show()
plt.close()



