from nimplus import Player, Game
import matplotlib.pyplot as plt

level = 9

alice = Player(trims = True)
bob = Player(prunes = True)
carl = Player()

game = Game(level, 1)

colors = [[],[]]
runs = 1000

for t in range(runs):

	game.play(alice, carl)
	game.play(bob, carl)

	blue = 0
	green = 0

	for i in alice.boxes:
		if alice.boxes[i] > alice.beads:
			if not i[1].winning():
				green += 1

	for i in bob.boxes:
		if bob.boxes[i] > bob.beads:
			if not i[1].winning():
				blue += 1

	colors[0].append(green)
	colors[1].append(blue)

x = [t for t in range(0, runs)]

plt.plot(x, colors[0], color = "green")
plt.plot(x, colors[1] , color = "blue") 
plt.show()
plt.close()



