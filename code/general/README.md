This folder only contains python code in .py and relative stocking folders (in case needed).

All of the files `.py` should be self-contained or refer to definitions in
```
	module/nimplus.py
```

(from now on NIM).

You can read about it in
```
	module/README.txt
```

Therefore do not cross-reference your code! If you think an important common function should be included in module/nim.py, consult matteoeo@gmail.com

-----------------------------------------------

File nomenclature:

- "indie_" is for code that does not use functions in NIM

- "det_" is for code that uses functions in NIM that do not need randomization

- "stoc_" is for code that uses randomization

- "_nim_" is for code regarding the traditional version of Nim, as opposed to Nim+ (which is assumed to be the standard)
