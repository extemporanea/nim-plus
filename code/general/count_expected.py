from nimplus import Player, State

class Counter:

	def __init__(self):
		self.games = 0
		self.turn = 0
		self.wins = 0
		self.moves = []
		self.prob = 1
		self.problist = []

def recursive(state):

	if counter.turn == 0:
		alice.set_boxes(state)
		boxes = alice.boxes
	else:
		bob.set_boxes(state)
		boxes = bob.boxes

	if state == finish:
		counter.games += 1
		counter.moves.append(counter.turn)
		if counter.turn % 2 == 1:
			counter.wins += 1
			counter.problist.append(counter.prob)
	else:
	
		counter.turn += 1
		
		state.set_avail()
		weights = [boxes[state, newstate] for newstate in state.avail]
		normalization = sum(weights)
		weights = [i/normalization for i in weights]
		
		for newstate in state.avail:
			i = state.avail.index(newstate)		
			counter.prob *= weights[i]
			recursive(newstate)
			counter.prob /= weights[i]			

		counter.turn -= 1

level = int(input("Level? [Default: 3] ") or "3")

if level >= 6:
	answer = input("You sure? This will probably get your computer stuck forever... [Y/N] ")
	if answer == "Y":
		pass
	else:
		quit()

alice = Player()
bob = Player()
counter = Counter()
start = State([1 for i in range(0, level)], plus = True)
finish = State(0)

recursive(start)

print('Number of games: ', counter.games)
print('Number of wins: ', counter.wins)
print('Average number of moves: ', sum(counter.moves)/len(counter.moves))
print('Average probability of victory: ', sum(counter.problist))










