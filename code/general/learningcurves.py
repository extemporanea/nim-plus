from nimplus import Player, Game
import matplotlib.pyplot as plt

level = 4

alice = Player(reinforces = True, beads = 10)
bob = Player()
game = Game(level, 1)

colors = [[],[]]
runs = 20000

for t in range(runs):

	game.play(alice, bob)

	red = 0
	green = 0

	for i in alice.boxes:
		if alice.boxes[i] > alice.beads:
			if i[1].winning():
				red += 1
			else:
				green += 1

	colors[0].append(red)
	colors[1].append(green)

# with open('run/data.txt', "w") as file:
#	file.write(str(colors))


x = [t for t in range(0, runs)]

plt.plot(x, colors[0], color = "red")
plt.plot(x, colors[1] , color = "green") 
plt.show()
plt.close()

ratio = []

for i in range(0, runs):
	ratio.append(colors[0][i]/colors[1][i])



