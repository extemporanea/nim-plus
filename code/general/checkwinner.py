from nimplus import Player, State, Game

'''For level 3, fast Nim+ game, input Alice's and Bob's boxes content then plays matches with random starter to decide winner with ... confidence.'''

matches = 100000

alice, bob = Player(), Player()

print("Alice's boxes:")

for i in range(8):

	fromstate = State(i)
	fromstate.set_avail()
	for tostate in fromstate.avail:
		alice.boxes[fromstate, tostate] = int(input(fromstate.name+' -> '+tostate.name+' : ')  or "5") 

print("Bob's boxes:")

for i in range(8):

	fromstate = State(i)
	fromstate.set_avail()
	for tostate in fromstate.avail:
		bob.boxes[fromstate, tostate] = int(input(fromstate.name+' -> '+tostate.name+' : ') or "5")

game = Game(3, matches)
game.play(alice,bob)

if alice.wins/matches > .6:
	print("Alice won!")
elif alice.wins/matches < .4:
	print("Bob won!")
else:
	print("It's a tie!")

