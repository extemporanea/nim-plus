import ffmpeg
import os
import glob


def make_video(fname, sim='pers'):
    try:
        os.remove(basepath+videopath+fname+'_'+sim+'.mp4')
    except FileNotFoundError:
        pass
    try:
        (
            ffmpeg
            .input(basepath+figspath+sim+'_*.png', pattern_type='glob', framerate=1)
            .output(basepath+videopath+fname+'_'+sim+'.mp4')
            .run()
        )
    except:
        print("Failed to generate the video")
        pass
    try:
        os.remove(basepath+figspath+sim+'_*.png')
    except:
        print("Failed to remove temporary figures")
        pass

