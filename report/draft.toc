\contentsline {section}{\numberline {1}Introduction}{4}%
\contentsline {section}{\numberline {2}Nim, Nim+, and some variants}{5}%
\contentsline {subsection}{\numberline {2.1}Third level: full analysis}{8}%
\contentsline {section}{\numberline {3}Workshop}{11}%
\contentsline {subsection}{\numberline {3.1}Reinforcement and pruning}{14}%
\contentsline {section}{\numberline {4}Coding the workshop}{14}%
\contentsline {section}{\numberline {5}Combinatorics}{14}%
\contentsline {subsection}{\numberline {5.1}Number of boxes (configurations)}{15}%
\contentsline {subsection}{\numberline {5.2}Number of colors}{19}%
\contentsline {subsection}{\numberline {5.3}Representing configurations: states}{20}%
\contentsline {subsection}{\numberline {5.4}Number of games}{21}%
\contentsline {section}{\numberline {6}Game theory}{23}%
\contentsline {subsection}{\numberline {6.1}Determinism}{24}%
\contentsline {subsection}{\numberline {6.2}Determining a state's winnability}{24}%
\contentsline {subsection}{\numberline {6.3}Winning moves}{29}%
\contentsline {subsection}{\numberline {6.4}Strategies}{29}%
\contentsline {subsection}{\numberline {6.5}Nim and Nim+: the bitwise XOR criterion}{29}%
\contentsline {subsection}{\numberline {6.6}AntiNim}{29}%
\contentsline {subsection}{\numberline {6.7}AntiNim+}{29}%
\contentsline {subsection}{\numberline {6.8}Relationship to traditional Nim}{36}%
\contentsline {section}{\numberline {7}Coding higher levels}{36}%
\contentsline {subsection}{\numberline {7.1}Optimizing the algorithm}{36}%
\contentsline {section}{\numberline {8}About machine learning}{36}%
\contentsline {section}{\numberline {9}Conclusions}{36}%
\contentsline {subsection}{\numberline {9.1}Pedagogical potentialities}{37}%
\contentsline {subsection}{\numberline {9.2}Scientific interest}{37}%
\contentsline {subsection}{\numberline {9.3}Literature review.}{37}%
\contentsline {section}{\numberline {10}About the authors}{37}%
\contentsline {section}{\numberline {11}Acknowledgements}{37}%
\contentsline {section}{\numberline {A}Learning workshop protocol}{39}%
\contentsline {section}{\numberline {B}Building workshop protocol}{40}%
\contentsline {section}{\numberline {C}Coding workshop protocol}{41}%
