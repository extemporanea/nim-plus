# Nim+ workflow

### Bibliography and sitography

- https://github.com/tkl5/Nim-Game
- Nimatron: http://platinumpiotr.blogspot.com/2016/01/1940-nimatron.html
- Master thesis: https://www.duo.uio.no/bitstream/handle/10852/73236/Solving-Nim-by-the-Use-of-Machine-Learning.pdf?sequence=1&isAllowed=y


### Possible destinations

- https://jcom.sissa.it/

- https://journals.aps.org/prper/

- https://scipost.org/journals/?field=multidisciplinary

- https://www.mdpi.com/journal/education

- etc. http://homepages.wmich.edu/~rudged/journals.html

(It might be split in two: lab activity and actual research. Regarding the latter, the element of novelty might be the comparison to an exactly solvable model.)

### To be done


- **Reading/Writing:** Read the manuscript, discuss, criticize, contribute.

- **Workshop:** Describe workshop practice and outcomes in a scholarly fashion, but with humor.

- **Workshop protocols:** Write protocol to be added in Appendix in a clear and to-the point fashion. Translate into other languages.

- **Algorithm/1:** Provide the shortest possible imperative algorithm that reproduces the workshop in pseudocode and/or in Scratch!!!

- **Math:** Improve on the combinatorics and/or on the strategy. Calculate fractal dimension of space of best moves.

- **Algorithm/2:**  Double-check/improve/comment the available code, arrange tools for online publication.

- **Algorithm/3:** Use available code for more refined data-analysis (speed and quality of learning etc.). Pictures of stochastic construction of the winning-move fractal.

- **Algorithm/4:** Interactive code for actual game playing with humans?

- **Algorithm/5:** Neural network algorithm to infer patterns in map bestmove, to explain analogies and differences between "savvy" ML and data extrapolation.

- **Comparison to "real" ML**: Analogies and differences between this approach and e.g. Neural Networks and other tools in Machine Learning.

- **Code documentation**: make jupiter notebook?

- **Pictures:** Produce better graphics for the pictures.

- **Exercises:** Write _Solution to exercises_ appendix.

- **Conclusions**: Add free comments on the social aspects of ML/AI etc.

- **Pedagogical tools:** Provide pedagogical tools to complement the manuscript in teaching activities.


- **Authors**:  Write _About the Authors_ section describing eXtemporanea as a whole in the light of the present work.

- **Language**: Review English; review inclusive language; translate protocols into Italian, Chinese, Hindu, etc.


### Author contributions

- **Workshop conduction:** [name=tomate] ...
- **Writing:** 
- **Coding:** 
- **Translation:** 
- **etc.**
